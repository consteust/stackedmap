# Stacked Map

This is an implementation of a stacked / wrapped / fallthrough map, in the same vein as JSPs and their "stacked" page/request/session/application scopes where a key lookup cycles through multiple levels of mappings to look up key values.

This basically provides set and map versions of that: you can name the collections as you push them so you can access them diretly (as you can with JSP), but a get of a key will descend the stack and return the first context key/value mapping it finds.

A key set will only be applied against the "top" map of the stack. If there are "lower" maps with that value, they are not modified. So mutations are only applied against the "top" map unless one gets a reference to a "lower" map.

note that size() and other normally-cheap operations become more expensive since a O(n) scan of all the keys of all the maps is necessary, and possibly worse big-O.

Those are maintained for operability with the Map and Set interfaces, especially with regards to various groovy collections operators, such as equality. 

There is no caching of the intermediate results. keySet() and values() are computed from the underlying stacked collections each time. 

There are no thread safe guarantees. Some synchronized keywords are used, but it could get dicey. Typically the "local execution" map will not be concurrently accessed, while the stacked global scopes would be, so the global scopes should be the appropriate threadsafe collection from Google Collections, Commons collections, and what have you

## artifacts

It's not in maven. If anyone uses it besides me I'll go through that but the gradle build for it is hellish. For now use Jitpack:

    <repository>
        <id>jitpack.io</id>
        <url>https://jitpack.io</url>
    </repository>

    <dependency>
        <groupId>com.github.carlemueller</groupId>
        <artifactId>stackedmap</artifactId>
        <version>v1.0.2</version>
    </dependency>


## Use cases

- As explained above, a situation where you need to mimic the "scoped levels" of JSP webapps (or even more complicated, Spring webflow)

- In recursion, you may have a scratchpad map for holding data, but you only want the changes set at any level as you "descend" say a tree to apply to nodes under the one you are currently at. But when the recursion returns back up the call stack, you want those changes discarded. You can use a stackedmap to wrap each level in a map.

- Although stackedmaps provide no guarantees of thread safety, they can nevertheless be used to handoff to other threads in a thread-safe sequential manner, with each thread adding to the original passed in map by wrapping the map and therefore appending their modification to the map rather than overwriting. Basically, since each thread only appends data in its local scope and doesn't modify the stacked/wrapped maps that came from other threads, they have their own thread-safe view on the overall map. These could be successively wrapped ImmutableMap collections for more guarantees of thread safety.

- Similarly to the first use case, you frequently can have two kinds of configuration data in an app: data that is checked into the core codebase: a universal set of settings/defaults, and local/environment-specific settins that can have lots of override settings or appending settings. Using stackedmaps you can load both and automatically apply the overrides in a natural manner.
