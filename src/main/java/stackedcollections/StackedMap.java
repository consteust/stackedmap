package stackedcollections;

import java.util.*;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * Stacked Maps allow people to wrap a map with another map (and wrap as many levels as needed).
 * <p>
 * Key lookups check the "topmost" map first and if not found, descend down the wrapped maps to look for the key
 * <p>
 * Key puts will be applied/stored only in the topmost map.
 * <p>
 * Implements all needed interfaces to the point it appears to just be a normal map to groovy/spock.
 *
 * @param <K> key type
 * @param <V> value type
 */
public class StackedMap<K, V> extends AbstractMap<K, V> implements Map<K, V> {
    protected Map<K, V> topMap = null;
    // TODO: protected String topScopeName = null
    protected LinkedBlockingDeque<Map<K, V>> deque = new LinkedBlockingDeque<>();
    protected Map<String, Map<K, V>> scopeIndex = null;

    // ---- constructors

    /**
     * creates new StackedMap with a single empty hashmap
     */
    public StackedMap() {
        push(new HashMap<>());
    }

    /**
     * creates a new StackedMap, either with default storage (initialized == true) or bare (initialized == false)
     *
     * @param initialized If true, provide a new HashMap as the base storage. If false, leave bare so it can be setup
     *                    with push() calls
     */
    public StackedMap(boolean initialized) {
        if (initialized) {
            push(new HashMap<>());
        }
    }

    /**
     * pushes the provided maps, pushing from first to last, builder-style (it returns the StackedMap instance so
     * more pushes can be chained)
     * <p>
     * (this may be counterintuitive as that will "reverse" the list of maps in terms of lookup order)
     *
     * @param maps
     */
    public StackedMap(Map<? extends K, ? extends V>... maps) {
        pushAll((Map[]) maps);
    }

    /**
     * pushes the provided maps, pushing from first to last, builder-style (it returns the StackedMap instance so
     * more pushes can be chained)
     *
     * @param m
     */
    public StackedMap(Map<? extends K, ? extends V> m) {
        push((Map) m);
    }

    public StackedMap(Map<? extends K, ? extends V> m, String scopeName) {
        push((Map) m, scopeName);
    }

    // ---- stacked map special methods

    public Map<K, V> pop() {
        if (topMap == null) {
            throw new IllegalStateException("stacked map has no maps pushed yet");
        }
        synchronized (deque) {
            Map<K, V> oldTopMap = topMap;
            deque.removeFirst();
            try {
                topMap = deque.getFirst();
            } catch (NoSuchElementException ex) {
                topMap = null;
            }
            return oldTopMap;
        }
    }

    public Map<K, V> top() {
        return topMap;
    }

    public StackedMap<K, V> push(Map<K, V> map) {
        if (map == null) {
            throw new IllegalArgumentException("Cannot push null into stacked map");
        }
        // TODO: improve thread access?
        synchronized (deque) {
            deque.addFirst(map);
            topMap = map;
        }
        return this;
    }

    /**
     * only push if the provided map is non-null
     * <p>
     * useful in some complex scope constructions
     *
     * @param map map that is pushed IF NOT NULL
     * @return
     */
    public StackedMap<K, V> pushIf(Map<K, V> map) {
        if (map == null) {
            return this;
        }
        // TODO: improve thread access?
        synchronized (deque) {
            deque.addFirst(map);
            topMap = map;
        }
        return this;
    }

    /**
     * only push if the provided map is non-null and the condition is true
     * <p>
     * useful in some complex scope constructions
     *
     * @param map map that is pushed IF NOT NULL
     * @param condition condition that must be true for the map to be pushed
     * @return
     */
    public StackedMap<K, V> pushIf(boolean condition, Map<K, V> map) {
        if (map == null || !condition) {
            return this;
        }
        // TODO: improve thread access?
        synchronized (deque) {
            deque.addFirst(map);
            topMap = map;
        }
        return this;
    }

    public StackedMap<K, V> pushAll(Map<K, V>... maps) {
        if (maps == null) {
            return this;
        }
        // TODO: improve thread access?
        synchronized (deque) {
            for (Map map : maps) {
                deque.addFirst(map);
                topMap = map;
            }
        }
        return this;
    }

    public StackedMap<K, V> push(Map<K, V> map, String scopeName) {
        if (scopeName == null) {
            throw new IllegalArgumentException("push of scoped map requires a non-null scope name");
        }
        // TODO: ?improve threading?
        synchronized (deque) {
            if (scopeIndex == null) {
                scopeIndex = new HashMap<>();
            }
            deque.addFirst(map);
            topMap = map;
            scopeIndex.put(scopeName, map);
        }
        return this;
    }

    public StackedMap<K, V> clearStack() {
        topMap = null;
        deque.clear();
        scopeIndex.clear();
        return this;
    }

    public Map<String, Map<K, V>> getScopes() {
        if (topMap == null) {
            throw new IllegalStateException("stacked map has no maps pushed yet");
        }
        if (scopeIndex == null) {
            throw new IllegalArgumentException("no named scopes specified for stacked map");
        }
        return scopeIndex;
    }

    // TODO: use less exceptions and just return nulls?
    public Map<K, V> getScope(String scopeName) {
        if (topMap == null) {
            throw new IllegalStateException("stacked map has no maps pushed yet");
        }
        if (scopeIndex == null) {
            throw new IllegalStateException("No named scopes have been specified for stacked map");
        }
        if (scopeIndex.containsKey(scopeName)) {
            return scopeIndex.get(scopeName);
        }
        throw new IllegalArgumentException("Scope " + scopeName + " not specified in stacked map");
    }

    // ---- java.util.Map methods

    @Override
    public V get(Object key) {
        if (topMap == null) {
            throw new IllegalStateException("stacked map has no maps pushed yet");
        }
        Iterator<Map<K, V>> iter = deque.iterator();
        while (iter.hasNext()) {
            Map<K, V> map = iter.next();
            if (map.containsKey(key)) {
                return map.get(key);
            }
        }
        return null;
    }

    @Override
    public boolean containsKey(Object key) {
        if (topMap == null) {
            throw new IllegalStateException("stacked map has no maps pushed yet");
        }
        Iterator<Map<K, V>> iter = deque.iterator();
        while (iter.hasNext()) {
            Map<K, V> map = iter.next();
            if (map.containsKey(key)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        if (topMap == null) {
            throw new IllegalStateException("stacked map has no maps pushed yet");
        }
        Iterator<Map<K, V>> iter = deque.iterator();
        while (iter.hasNext()) {
            Map<K, V> map = iter.next();
            if (map.containsValue(value)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Collection<V> values() {
        if (topMap == null) {
            throw new IllegalStateException("stacked map has no maps pushed yet");
        }
        List<V> vals = new ArrayList<>();
        Iterator<Map<K, V>> i = deque.iterator();
        while (i.hasNext()) {
            Map<K, V> m = i.next();
            vals.addAll(m.values());
        }
        return vals;

    }

    @Override
    public int size() {
        if (topMap == null) {
            throw new IllegalStateException("stacked map has no maps pushed yet");
        }
        return keySet().size();
    }

    @Override
    public Set<K> keySet() {
        if (topMap == null) {
            throw new IllegalStateException("stacked map has no maps pushed yet");
        }
        StackedSet<K> ss = new StackedSet<>();
        Iterator<Map<K, V>> i = deque.descendingIterator();
        while (i.hasNext()) {
            ss.push(i.next().keySet());
        }
        return ss;
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        if (topMap == null) {
            throw new IllegalStateException("stacked map has no maps pushed yet");
        }
        Set<Entry<K, V>> entries = new HashSet<>();

        for (K key : keySet()) {
            entries.add(new AbstractMap.SimpleEntry<>(key, get(key)));
        }
        return entries;
    }

    @Override
    public V put(K key, V value) {
        if (topMap == null) {
            throw new IllegalStateException("stacked map has no maps pushed yet");
        }
        return topMap.put(key, value);
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        if (topMap == null) {
            throw new IllegalStateException("stacked map has no maps pushed yet");
        }
        for (Entry<? extends K, ? extends V> entry : m.entrySet()) {
            put(entry.getKey(), entry.getValue());
        }
    }

    @Override
    public V remove(Object key) {
        if (topMap == null) {
            throw new IllegalStateException("stacked map has no maps pushed yet");
        }
        return topMap.remove(key);
    }

    @Override
    public void clear() {
        if (topMap == null) {
            throw new IllegalStateException("stacked map has no maps pushed yet");
        }
        topMap.clear();
    }


    /**
     * Static factory/build method for a stacked map with multiple maps
     * <p>
     * (this may be counterintuitive as that will "reverse" the list of maps in terms of lookup order)
     *
     * @param maps
     * @return
     */
    public static StackedMap build(Map... maps) {
        StackedMap stack = new StackedMap<>((Map[]) maps);
        return stack;
    }

    /**
     * Utility method to stack maps (which is useful for inherited scopes and other applications)
     * <p>
     * For example:
     *
     * <pre>
     * {@code
     * Map globalScope = new HashMap<>();
     * StackedMap appScope = StackedMap.stack(globalScope);  // stack of new HashMap atop globalScope
     * StackedMap pageScope = StackedMap.stack(appScope); // stacke of a new HashMap atop appscope, which is atop globalscope
     * }
     * </pre>
     *
     * @param map
     * @return
     */
    public static StackedMap stack(Map map) {
        StackedMap stack = new StackedMap<>(map);
        stack.push(new HashMap<>());
        return stack;
    }


}
