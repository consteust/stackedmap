package stackedcollections

import spock.lang.Specification;

class StackedMapSpec extends Specification {

    void 'test four-level stacking of maps'() {
        given:
        Map one = ["a": "AA", "b": "BB"]
        Map two = ["1": "aaa", "2": "aaa"]
        Map three = ["1": "baa", "2": "baa"]
        Map four = ["1": "aaz"]

        when: 'we push map "two" atop map "one"'
        StackedMap sm = new StackedMap(one)
        sm.push(two)

        then:
        sm["1"] == "aaa"
        sm["2"] == "aaa"
        sm["a"] == "AA"
        sm["b"] == "BB"

        when: 'we push another map atop the stack'
        sm.push(three)

        then:
        sm["1"] == "baa"

        when: 'we push yet another map'
        sm.push(four)

        then:
        sm["1"] == "aaz"
        sm.keySet() == ["a", "b", "1", "2"] as Set
        sm == ["a": "AA", "b": "BB", "1": "aaz", "2": "baa"]

        when:
        sm["z"] = "ZZ"

        then:
        sm == ["a": "AA", "b": "BB", "1": "aaz", "2": "baa", "z": "ZZ"]

        when: 'we pop the top map off'
        sm.pop()

        then:
        sm == ["a": "AA", "b": "BB", "1": "baa", "2": "baa"]
    }
}
