package stackedcollections

import spock.lang.Specification;

class StackedSetSpec extends Specification {

    void 'test stacked map'() {

        when:
        Set stacked = ["ball"] as StackedSet

        then:
        stacked == ["ball"] as TreeSet

        when:
        Set setA = ["1", "A", "B"] as HashSet
        Set setB = ["11", "aa", "A", "bb"] as HashSet
        StackedSet ss = new StackedSet()
        ss.push(setA, "setA")
        ss.push(["A"] as Set)
        ss.push(["A"] as Set)
        ss.push([] as Set)
        ss.push(setB)

        then:
        ss.contains("B")
        ss.contains("bb")

        when:
        ss.add("cc")

        then:
        ss.contains("cc")

        when:
        ss.getScope("setA").add("dd")

        then:
        ss.contains("dd")

        when:
        Set setLit = ["11", "aa", "bb", "cc", "A", "dd", "1", "B"] as Set
        Set setTree = ["11", "aa", "bb", "cc", "A", "dd", "1", "B"] as TreeSet
        Set setHash = ["11", "aa", "bb", "cc", "A", "dd", "1", "B"] as HashSet
        Set empty = [] as Set
        Set addedSet = empty + ss

        then:
        ss == setLit
        ss == addedSet
        ss == setTree
        ss == setHash
        setTree == setLit
        setTree == setHash
        setHash == setLit
    }

    void 'testGroovyIntegrationPlusEqual'() {
        given:
        Set setA = ["1", "A", "B"] as HashSet
        Set setB = ["11", "aa", "A", "bb"] as HashSet
        StackedSet ss = new StackedSet()

        when:
        ss.push(setA, "setA")
        ss.push(["A"] as Set)
        ss.push(["A"] as Set)
        ss.push([] as Set)
        ss.push(setB)
        ss += "aaa"


        then:
        "aaa" in ss
    }
}
